# Artiklivaramu

Ühtsed artiklistandardid:
- sisuartikli standard
- teenuseartikli standard (TODO)
- kontaktiartikli standard (TODO)
- kataloogiartikli standard (TODO)

## Sisuartikli standard v1

PoC lahenduse puhul juhindutakse olemasolevast võimekusest, mida sammhaaval laiendatakse.

| Sisuelement                      | Kommentaar                                            | Atribuut sisuartiklis (MD/YAML) | Täitmise kohustuslikkus |
|----------------------------------|-------------------------------------------------------|---------------------------------|-------------------------|
| Pealkiri                         | Sisuartikli pealkiri                                  | title                           | JAH                     |
| Asutus                           | Sisuartikli eest vastutav asutus                      | text_by                         | JAH                     |
| Kuupäev                          | Artikli viimase muutmise kuupäev                      | date                            | JAH                     |
| -                                | (Süsteemne atribuut)                                  | layout: page                    | -                       |
| Võtmesõnad                       | Sisuartikliga seonduvad võtmesõnad                    | metadata: keywords              | JAH                     |
| Kirjeldus                        | Sisuartikli kirjeldus                                 | metadata: description           | EI                      |
| Teenused                         | Nimekiri sisuartikliga seotud teenustest              | services                        | EI                      |
| Teenused: teenuse nimi           |                                                       | services: text                  | EI                      |
| Teenused: viide teenusele        |                                                       | services: url                   | EI                      |
| -                                | (Süsteemne atribuut eesti.ee-s nähtavuse haldamiseks) | services: visible               | -                       |                 |
| Dokumendid                       | Nimekiri sisuartikliga seotud dokumentidest           | documents                       | EI                      |
| Dokumendid: dokumendi nimetus    | Näide: Euroopa ravikindlustuskaardi avaldus           | documents: text                 | EI                      |
| Dokumendid: viide dokumendile    | Näide: https://www.haigekassa.ee/inimesele/avaldused  | documents: url                  | EI                      |
| -                                | (Süsteemne atribuut eesti.ee-s nähtavuse haldamiseks) | documents: visible              | -                       |
| Seadused                         | Nimekiri sisuartikliga seotud seadustest              | laws                            | EI                      |
| Seadused: seaduse nimi           |                                                       | laws: text                      | EI                      |
| Seadused: viide seadusele        |                                                       | laws: url                       | EI                      |
| -                                | (Süsteemne atribuut eesti.ee-s nähtavuse haldamiseks) | laws: visible                   | -                       |
| Seotud artiklid                  | Nimekiri sisuartikliga seotud eesti.ee artiklitest    | related-articles                | EI                      |
| Seotud artiklid: pealkiri        |                                                       | related-articles: text          | EI                      |
| Seotud artiklid: viide artiklile |                                                       | related-articles: url           | EI                      |
| -                                | (Süsteemne atribuut eesti.ee-s nähtavuse haldamiseks) | related-articles: visible       | -                       |
| Seotud asutused                  | Nimekiri sisuartikliga seotud asutustest              | related-institutions            | JAH                     |
| Seotud asutused: asutuse nimi    |                                                       | related-institutions: name      | JAH                     |
| Seotud asutused: viide asutusele |                                                       | related-institutions: url       | JAH                     |
| -                                | (Süsteemne atribuut eesti.ee-s nähtavuse haldamiseks) | related-institutions: visible   | -                       |
| Viited                           | Nimekiri seotud infot pakkuvatest lehtedest           | references                      | EI                      |
| Viited: infotekst                | Näide: Ravikindlustus (haigekassa)                    | references: text                | EI                      |
| Viited: infoteksti viide         | Näide: https://www.haigekassa.ee/inimesele/kindlustus | references: url                 | EI                      |
| -                                | (Süsteemne atribuut eesti.ee-s nähtavuse haldamiseks) | references: visible             | -                       |
| -                                | (Süsteemne atribuut - püsiv link artikli kirjele)     | permalink                       | -                       |
|                                  | (Süsteemne atribuut - artikli keeletunnus)            | lang                            | -                       |

## Sisuartikli standard vX

Siin on eelduseks:
- (Teenused) : vähemalt sisuartiklite ulatuses viidatavad teenused on Teenuse kataloogis tutvustatud


| Sisuelement                      | Kommentaar                                            | Atribuut sisuartiklis (MD/YAML) | Täitmise kohustuslikkus |
|----------------------------------|-------------------------------------------------------|---------------------------------|-------------------------|
| Pealkiri                         | Sisuartikli pealkiri                                  | title                           | JAH                     |
| Asutus                           | Sisuartikli eest vastutav asutus                      | text_by                         | JAH                     |
| Kuupäev                          | Artikli viimase muutmise kuupäev                      | date                            | JAH                     |
| -                                | (Süsteemne atribuut)                                  | layout: page                    | -                       |
| Võtmesõnad                       | Sisuartikliga seonduvad võtmesõnad                    | metadata: keywords              | JAH                     |
| Kirjeldus                        | Sisuartikli kirjeldus                                 | metadata: description           | EI                      |
| Teenused                         | Nimekiri sisuartikliga seotud teenustest              | services                        | EI                      |
| Teenused: teenuse URI            | Link teenuse kataloogi kirjele                        | services: service_uri           | EI                      |
| -                                | (Süsteemne atribuut eesti.ee-s nähtavuse haldamiseks) | services: visible               | -                       |
| Dokumendid                       | Nimekiri sisuartikliga seotud dokumentidest           | documents                       | EI                      |
| Dokumendid: dokumendi nimetus    | Näide: Euroopa ravikindlustuskaardi avaldus           | documents: text                 | EI                      |
| Dokumendid: viide dokumendile    | Näide: https://www.haigekassa.ee/inimesele/avaldused  | documents: url                  | EI                      |
| -                                | (Süsteemne atribuut eesti.ee-s nähtavuse haldamiseks) | documents: visible              | -                       |
| Seadused                         | Nimekiri sisuartikliga seotud seadustest              | laws                            | EI                      |
| Seadused: seaduse nimi           |                                                       | laws: text                      | EI                      |
| Seadused: viide seadusele        |                                                       | laws: url                       | EI    
| -                                | (Süsteemne atribuut eesti.ee-s nähtavuse haldamiseks) | laws: visible                   | -                       |
| Seotud artiklid                  | Nimekiri sisuartikliga seotud eesti.ee artiklitest    | related-articles                | EI                      |
| Seotud artiklid: pealkiri        |                                                       | related-articles: text          | EI                      |
| Seotud artiklid: viide artiklile |                                                       | related-articles: url           | EI                      |
| -                                | (Süsteemne atribuut eesti.ee-s nähtavuse haldamiseks) | related-articles: visible       | -                       |
| Seotud asutused                  | Nimekiri sisuartikliga seotud asutustest              | related-institutions            | JAH                     |
| Seotud asutused: asutuse nimi    |                                                       | related-institutions: name      | JAH                     |
| Seotud asutused: viide asutusele |                                                       | related-institutions: url       | JAH                     |
| -                                | (Süsteemne atribuut eesti.ee-s nähtavuse haldamiseks) | related-institutions: visible   | -                       |
| Viited                           | Nimekiri seotud infot pakkuvatest lehtedest           | references                      | EI                      |
| Viited: infotekst                | Näide: Ravikindlustus (haigekassa)                    | references: text                | EI                      |
| Viited: infoteksti viide         | Näide: https://www.haigekassa.ee/inimesele/kindlustus | references: url                 | EI                      |
| -                                | (Süsteemne atribuut eesti.ee-s nähtavuse haldamiseks) | references: visible             | -                       |
| -                                | (Süsteemne atribuut - püsiv link artikli kirjele)     | permalink                       | -                       |
|                                  | (Süsteemne atribuut - artikli keeletunnus)            | lang                            | -                       |
| RIHA viide                       | Viide vastavale RIHA teenusele                        | riha_reference                  | EI                      |

### Sisuartikli mitteavalikud metaväljad

Sisuartiklite eest vastutavate asutuste kõrval on vajalik pidada arvet ka sisuartiklite autorite osas:
- et tagada vastutavate isikute muutumisel (nt: töökoha vahetus) automaatselt võimalike metaväljade parandus ja vajadusel teavitada asutust vajadusest
määrata uus teabevaldaja.

| Sisuelement                      | Kommentaar                                            | Atribuut sisuartiklis (MD/YAML) | Täitmise kohustuslikkus |
|----------------------------------|-------------------------------------------------------|---------------------------------|-------------------------|
| Autor                            | Vastutav artikli autor                                | title                           | -                       |
| Autor: isikukood                 |                                                       | author: name                    | -                       |
| Autor: nimi                      | Eesnimi + perenimi                                    | author: name                    | -                       |
| Autor: email                     |                                                       | author: email                   | -                       |
| Autor: ametikoht                 |                                                       | author: position                | -                       |
